﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio4
{
    public partial class Form1 : Form
    {
        int contador = 0;
        List<string> parejas = new List<string>();
        List<PictureBox> box = new List<PictureBox>();
        Timer tiempo = new Timer();
        Timer tiempo2 = new Timer();
        Timer tiempo3 = new Timer();
        int contadorparejas = 0;
        int segundos = 0, minutos = 0;
        int contador2 = 0;
        List<Jugador> jugadores = new List<Jugador>();

        public Form1()
        {
            InitializeComponent();
            tiempo.Tick += new EventHandler(Cronometro);
            tiempo2.Tick += new EventHandler(Cronometro2);
            tiempo3.Tick += new EventHandler(Cronometro3);
            int i, j;
            int altura = 10;
            int fila = 20, numero = 0;
            tiempo2.Interval = 1000;
            tiempo2.Start();
            Jugadores();

            string[] imagenes = Directory.GetFiles(/*Directory.GetCurrentDirectory()*/"imagenes");
            List<string> final = new List<string>();

            for (i = 0; numero < imagenes.Length * 2; i++)
            {
                final.Add(imagenes[i]);
                numero++;
                if (i == imagenes.Length - 1)
                {
                    i = -1;
                }
            }

            Random r = new Random();
            int posicion;
            for (i = 1; i <= 3; i++)
            {
                for (j = 1; j <= 4; j++)
                {
                    posicion = r.Next(0, final.Count);
                    PictureBox pt = new PictureBox();
                    pt.Size = new Size(128, 128);
                    pt.Location = new Point(fila, altura);
                    pt.Text = Convert.ToString(final[posicion]);
                    pt.Click += new EventHandler(PictureBox_Click);
                    final.RemoveAt(posicion);
                    //pt.Visible = false;
                    panel1.Controls.Add(pt);
                    //pictures.Add(pt);
                    fila = fila + 135;
                    pt.BorderStyle = BorderStyle.FixedSingle;
                }

                altura = altura + 135;
                fila = 20;
            }
            this.Text = "Juego de las Parejas";
        }

        private void PictureBox_Click(object sender, EventArgs e)
        {
            contador++;


            PictureBox pt = (PictureBox)sender;

            pt.Image = Image.FromFile(pt.Text);


            if (contador == 1)
            {
                //pt.Visible = true;
                parejas.Add(pt.Text);
                pt.Enabled = false;
                box.Add(pt);
                pt.BorderStyle = BorderStyle.Fixed3D;
            }
            else
            {
                pt.Enabled = false;
                pt.BorderStyle = BorderStyle.Fixed3D;
                //pt.Visible = true;
                parejas.Add(pt.Text);
                box.Add(pt);
                tiempo.Interval = 500;
                tiempo.Start();

                panel1.Enabled = false;
                contador = 0;
            }

        }

        private void Cronometro(Object myObject, EventArgs myEventArgs)
        {
            tiempo.Stop();

            if (parejas[0] == parejas[1])
            {
                box[0].Visible = false;
                box[1].Visible = false;
                contadorparejas++;
                if(contadorparejas == 6)
                {
                    tiempo2.Stop();
                    //MessageBox.Show("Has Ganado En " + minutos + " minutos y " + segundos + " segundos");
                    //label1.Visible = false;
                    //this.Close();
                    //ActiveForm.Enabled = false;
                    panel1.Enabled = false;
                    tiempo3.Interval = 500;
                    tiempo3.Start();

                    panel1.Controls.Clear();
                    Label l = new Label();
                    l.Size = new Size(200, 30);
                    l.Text = "Escribe tu nombre";
                    l.TextAlign = ContentAlignment.MiddleCenter;
                    l.Location = new Point(170, 170);
                    panel1.Controls.Add(l);

                    TextBox t = new TextBox();
                    t.Size = new Size(200, 30);
                    t.Location = new Point(170, 200);
                    panel1.Controls.Add(t);

                    Button bt = new Button();
                    bt.Size = new Size(100, 30);
                    bt.Text = "Aceptar";
                    bt.Location = new Point(220, 220);
                    bt.Click += new EventHandler(Button_Click);
                    panel1.Controls.Add(bt);



                    //Añadir jugadores de la lista mas el que el nombre y tiempo del ultimo poner boton antes y un texbos para el nombre crear jugador y añadirlo a la lista 
                    //ordenarlos todos y colocarlos en la lista en orden con un for
                    //Al final guardar el archivo
                    
                }
            }
            else
            {
                box[0].Image = null;
                box[1].Image = null;
                box[0].BorderStyle = BorderStyle.FixedSingle;
                box[1].BorderStyle = BorderStyle.FixedSingle;
            }
            box[0].Enabled = true;
            box[1].Enabled = true;
            panel1.Enabled = true;
            parejas.Clear();
            box.Clear();

        }

        private void Cronometro2(Object myObject, EventArgs myEventArgs)
        {
            segundos++;

            if (segundos == 60)
            {
                minutos++;
                //milisegundos = 0;
                segundos = 0;
            }
            //else
            //{
            //    if(minutos == 60)
            //    {
            //        minutos++;
            //        segundos = 0;
            //    }
            //}

            label1.Text = minutos.ToString().PadLeft(2, '0') + ":" + segundos.ToString().PadLeft(2, '0');

        }

        private void Button_Click(Object myObject, EventArgs myEventArgs)
        {
            string tiempo = label1.Text;
            string nombre = panel1.Controls[1].Text;

            Jugador j = new Jugador(nombre, tiempo);
            jugadores.Add(j);
            AñadeJugadores();
            panel1.Controls.Clear();

            ListView pt = new ListView();
            pt.Size = new Size(200, 400);
            pt.View = View.Tile;
            pt.Location = new Point(200, 10);
            panel1.Controls.Add(pt);

            List<Jugador> listaView = new List<Jugador>();

            OrdenaLista();

            for (int i = 0; i < jugadores.Count; i++)
            {
                //pt.Font = new Font(FontFamily.GenericSansSerif, 12.0F, FontStyle.Bold);
                pt.Items.Add(jugadores[i].Nombre);
                //pt.Font = new Font(FontFamily.GenericSansSerif, 8.25F, FontStyle.Regular); 
                pt.Items.Add(jugadores[i].Tiempo);
                pt.Items.Add("=======================");       
            }
        }

        private void Cronometro3(Object myObject, EventArgs myEventArgs)
        {
            contador2++;

            if(contador2 % 2 == 0)
            {
                label1.Visible = true;
            }
            else
            {
                label1.Visible = false;
            }
            
            
        }

        private void Jugadores()
        {
            FileStream fs = new FileStream("Puntuaciones.txt", FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);
            string nombre;
            string tiempo;

            while (!sr.EndOfStream)
            {
                nombre = sr.ReadLine();
                tiempo = sr.ReadLine();

                Jugador j = new Jugador(nombre, tiempo);

                jugadores.Add(j);
            }

            sr.Close();
            fs.Close();
        }

        private void AñadeJugadores()
        {
            int i;
            StreamWriter sw = new StreamWriter("Puntuaciones.txt");

            for(i = 0; i < jugadores.Count - 1; i++)
            {
                sw.WriteLine(jugadores[i].Nombre);
                sw.WriteLine(jugadores[i].Tiempo);
            }

            sw.WriteLine(jugadores[i].Nombre);
            sw.Write(jugadores[i].Tiempo);

            sw.Close();
        }

        public void OrdenaLista()
        {
            int i, posjugador = 0;
            List<Jugador> l2 = new List<Jugador>();

            while (jugadores.Count > 0)
            {
                string tiempo = jugadores[0].Tiempo;
                string[] tiempo2 = tiempo.Split(':');
                int minutos = int.Parse(tiempo2[0]);
                int segundos = int.Parse(tiempo2[1]);
                posjugador = 0;

                for (i = 1; i < jugadores.Count; i++)
                {
                    string tiempo3 = jugadores[i].Tiempo;
                    string[] tiempo4 = tiempo3.Split(':');
                    int minutos2 = int.Parse(tiempo4[0]);
                    int segundos2 = int.Parse(tiempo4[1]);

                    if (minutos2 < minutos || minutos == minutos2 && segundos2 < segundos)
                    {
                        posjugador = i;
                        minutos = minutos2;
                        segundos = segundos2;
                    }  
                }
                l2.Add(jugadores[posjugador]);
                jugadores.RemoveAt(posjugador);
            }
            jugadores.AddRange(l2);
        }
    }
}
