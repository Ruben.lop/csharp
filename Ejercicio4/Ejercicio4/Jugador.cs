﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio4
{
    class Jugador
    {
        private string nombre;
        private string tiempo;

        public Jugador(string nombre, string tiempo)
        {
            this.nombre = nombre;
            this.tiempo = tiempo;
        }

        public string Nombre
        {
            get
            {
                return this.nombre;
            }
        }

        public string Tiempo
        {
            get
            {
                return this.tiempo;
            }
        }
    }
}
