﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio3
{
    class Pokemon
    {
        private int id;
        private string nombre;
        private string tipo;
        private int ataque;
        private int defensa;
        private int vida;
        private int ataqueespecial;
        private int defensaespecial;
        private int velocidad;
        private string abilidad;
        private bool capturado;

        public Pokemon(int id, string nombre, string tipo, int ataque, int defensa, int vida, int ataqueespecial, int defensaespecial, int velocidad, string abilidad, bool capturado)
        {
            this.id = id;
            this.nombre = nombre;
            this.tipo = tipo;
            this.ataque = ataque;
            this.defensa = defensa;
            this.vida = vida;
            this.ataqueespecial = ataqueespecial;
            this.defensaespecial = defensaespecial;
            this.velocidad = velocidad;
            this.abilidad = abilidad;
            this.capturado = capturado;

            //Pokemon p = new Pokemon(id, nombre, tipo, ataque, defensa, vida, ataqueespecial, defensaespecial, velocidad, abilidad, capturado);
        }

        public int Id
        {
            get
            {
                return this.id;
            }
        }

        public string Nombre
        {
            get
            {
                return this.nombre;
            }
        }

        public int Ataque
        {
            get
            {
                return this.ataque;
            }
        }

        public int Ataqueespecial
        {
            get
            {
                return this.ataqueespecial;
            }
        }

        public int Defensa
        {
            get
            {
                return this.defensa;
            }
        }

        public int Defensaespecial
        {
            get
            {
                return this.defensaespecial;
            }
        }

        public int Vida
        {
            get
            {
                return this.vida;
            }
        }

        public string Tipo
        {
            get
            {
                return this.tipo;
            }
        }

        public int Velocidad
        {
            get
            {
                return this.velocidad;
            }
        }

        public string Abilidad
        {
            get
            {
                return this.abilidad;
            }
        }

        public bool Capturado
        {
            get
            {
                return this.capturado;
            }
            set
            {
                capturado = value;
            }
        }

        public override string ToString()
        {
            string cadena = "Id: " + id + "\n" + "Nombre: " + nombre + "\n" + "Tipo: " + tipo + "\n" + "Ataque: " + ataque + "\n" + "Defensa: " + defensa + "\n" + "Vida: "+ vida + "\n" + "Ataque Especial: " + ataqueespecial + "\n" + "Defensa Especial: " + defensaespecial + "\n" + "Velocidad: " + velocidad + "\n" + "Abilidad: " + abilidad + "\n" + "Capturado: " + capturado;

            //string captura = "";

            //if (capturado)
            //{
            //    Console.ForegroundColor = ConsoleColor.Blue;
            //    captura = "Sí";
            //}
            //else
            //{
            //    Console.ForegroundColor = ConsoleColor.Red;
            //    captura = "No";
            //}
            //Console.ForegroundColor = ConsoleColor.Gray;

            //cadena = cadena + "Capturado: " + capturado;

            return cadena;
        }
    }
}
