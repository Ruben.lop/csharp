﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio3
{
    class Pokedex
    {
        private List<Pokemon> pokedex;

        public Pokedex()
        {
            pokedex = new List<Pokemon>();
        }

        public Pokedex(string nombreFichero)
        {
            pokedex = new List<Pokemon>();

            StreamReader sr = new StreamReader(nombreFichero);
            string[] atributos = { "" };
            string linea;
            int id;
            string nombre;
            string tipo;
            int ataque;
            int defensa;
            int vida;
            int ataqueespecial;
            int defensaespecial;
            int velocidad;
            string abilidad;
            bool capturado;

            while (!sr.EndOfStream)
            {
                linea = sr.ReadLine();
                atributos = linea.Split(',');

                id = Convert.ToInt32(atributos[0]);
                nombre = atributos[1];
                tipo = atributos[2];
                ataque = Convert.ToInt32(atributos[3]);
                defensa = Convert.ToInt32(atributos[4]);
                vida = Convert.ToInt32(atributos[5]);
                ataqueespecial = Convert.ToInt32(atributos[6]);
                defensaespecial = Convert.ToInt32(atributos[7]);
                velocidad = Convert.ToInt32(atributos[8]);
                abilidad = atributos[9];
                capturado = Convert.ToBoolean(atributos[10]);

                Pokemon p = new Pokemon(id, nombre, tipo, ataque, defensa, vida, ataqueespecial, defensaespecial, velocidad, abilidad, capturado);

                pokedex.Add(p);
            }
            sr.Close();
        }

        public void LeeCSV(string nombreFichero)
        {
            pokedex.Clear();
            int i;
            StreamReader sr = new StreamReader(nombreFichero);
            string[] atributos = { "" };
            string linea;
            int id;
            string nombre;
            string tipo;
            int ataque;
            int defensa;
            int vida;
            int ataqueespecial;
            int defensaespecial;
            int velocidad;
            string abilidad;
            bool capturado;

            while (!sr.EndOfStream)
            {
                linea = sr.ReadLine();
                atributos = linea.Split(';');

                id = Convert.ToInt32(atributos[0]);
                nombre = atributos[1];
                tipo = atributos[2];
                ataque = Convert.ToInt32(atributos[3]);
                defensa = Convert.ToInt32(atributos[4]);
                vida = Convert.ToInt32(atributos[5]);
                ataqueespecial = Convert.ToInt32(atributos[6]);
                defensaespecial = Convert.ToInt32(atributos[7]);
                velocidad = Convert.ToInt32(atributos[8]);
                abilidad = atributos[9];
                capturado = Convert.ToBoolean(atributos[10]);

                Pokemon p = new Pokemon(id, nombre, tipo, ataque, defensa, vida, ataqueespecial, defensaespecial, velocidad, abilidad, capturado);

                pokedex.Add(p);
            }
            sr.Close();
        }

        public void CapturaPokemon(string nombre)
        {
            int i;

            for (i = 0; i < pokedex.Count; i++)
            {
                if(pokedex[i].Nombre.ToLower() == nombre.ToLower())
                {
                    pokedex[i].Capturado = true;
                }
            }
        }

        public double PorcentajeCapturas()
        {
            int i, capturados = 0;
            double media;

            for (i = 0; i < pokedex.Count; i++)
            {
                if (pokedex[i].Capturado)
                {
                    capturados++;
                }
            }
            
            media = ((double)capturados / (double)pokedex.Count) * 100;

            return Math.Round(media, 2);
        }

        public Pokemon BuscaPokemon(int id)
        {
            int i, posicion = 0;

            for (i = 0; i < pokedex.Count; i++)
            {
                if (pokedex[i].Id == id)
                {
                    posicion = i;
                }
            }

            return pokedex[posicion];
        }

        public Pokemon BuscaPokemon(string nombre)
        {
            int i, posicion = 0;

            for (i = 0; i < pokedex.Count; i++)
            {
                if (pokedex[i].Nombre.ToLower() == nombre.ToLower())
                {
                    posicion = i;
                }
            }

            return pokedex[posicion];
        }

        public void GuardaCSV(string nombreFichero)
        {
            int i/*, contador = 0*/;

            StreamWriter sw = new StreamWriter(nombreFichero);

            for (i = 0; i < pokedex.Count; i++)
            {
                sw.Write(pokedex[i].Id + ",");
                sw.Write(pokedex[i].Nombre + ",");
                sw.Write(pokedex[i].Tipo + ",");
                sw.Write(pokedex[i].Ataque + ",");
                sw.Write(pokedex[i].Defensa + ",");
                sw.Write(pokedex[i].Vida + ",");
                sw.Write(pokedex[i].Ataqueespecial + ",");
                sw.Write(pokedex[i].Defensaespecial + ",");
                sw.Write(pokedex[i].Velocidad + ",");
                sw.Write(pokedex[i].Abilidad + ",");
                sw.WriteLine(pokedex[i].Capturado);

                //if(contador == 2)
                //{
                //    sw.WriteLine(false);
                //}
                //else
                //{
                //    sw.WriteLine(true);
                //}
                //contador++;

                //if (contador == 3)
                //{
                //    contador = 0;
                //}
            }

            sw.Close();
        }

    }
}
