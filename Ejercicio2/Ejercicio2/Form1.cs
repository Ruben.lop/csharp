﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio2
{
    public partial class Form1 : Form
    {
        List<string> palabras;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            palabras = new List<string>();
            button1.Enabled = false;

            //Con el otro fichero con palabras se podra comprobar si esta una palabra esta en la cadena comparandola con trozos de la cadena de igual tamaño de dicha palabra.
            //pero no se como cortar cada palabra ya que vienen todas juntas y es un fichero de texto. MIRAR SI ES UN ERROR !!!!

            StreamReader sr = new StreamReader("palabras.txt");

            while (!sr.EndOfStream)
            {
                palabras.Add(sr.ReadLine().ToLower());
            }

            sr.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if(textBox1.Text == "")
            {
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }
        }

        private void abrirFicheroDeTextoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult d;

            d = openFileDialog1.ShowDialog();

            if(d == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(openFileDialog1.FileName);
                textBox1.Text = sr.ReadToEnd();

                sr.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();

            int i;
            string cadena = textBox1.Text;
            cadena = QuitaCaracter(cadena);
            cadena = QuitaEspacios(cadena);

            cadena = cadena.ToLower();

            

            //textBox1.Text = cadena;

            string[] texto = cadena.Split(' ');

            List<string> lista = new List<string>();

            for (i = 0; i < texto.Length; i++)
            {
                if (!palabras.Contains(texto[i]) && !lista.Contains(texto[i]))
                {
                    listBox1.Items.Add(texto[i]);
                    lista.Add(texto[i]);
                }
            }
        }

        private static string QuitaEspacios(string cadena)
        {
            return string.Join(" ", cadena.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
        }
        private static string QuitaCaracter(string cadena)
        {
            int i;
            string cadena2 = "";

            for (i = 0; i < cadena.Length; i++)
            {
                if (char.IsLetter(cadena[i]) || cadena[i] == ' ')
                {
                    cadena2 = cadena2 + cadena[i];
                }
                else
                {
                    if(cadena[i] == '\n')
                    {
                        cadena2 = cadena2 + " ";
                    }
                }
            }

            return cadena2;
        }
    }
}
