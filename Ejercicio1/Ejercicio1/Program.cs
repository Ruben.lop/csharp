﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{
    class Program
    {
        static int[] CalculaDigitosControl(int[] a)
        {
            if(a.Length == 20)
            {
                int i, suma = 0, division, mul;
                //int[] primerodigitos = new int[10];
                int[] multiplica = { 1, 2, 4, 8, 5, 10, 9, 7, 3, 6 };
                //primerodigitos[0] = 0;
                //primerodigitos[1] = 0;
                for (i = 0; i < 8; i++)
                {
                    mul = a[i] * multiplica[i + 2];
                    suma = suma + mul; 
                }
                division = suma % 11;
                int primerdigito = 11 - division;

                if(primerdigito == 10)
                {
                    primerdigito = 1;
                }
                else
                {
                    if(primerdigito == 11)
                    {
                        primerdigito = 0;
                    }
                }

                int[] final = new int[2];

                final[0] = primerdigito;
                int j;

                suma = 0;

                for (i = 10, j = 0; i < a.Length; i++, j++)
                {
                    mul = a[i] * multiplica[j];
                    suma = suma + mul;
                }

                division = suma % 11;
                int segundodigito = 11 - division;

                if (segundodigito == 10)
                {
                    segundodigito = 1;
                }
                else
                {
                    if (segundodigito == 11)
                    {
                        segundodigito = 0;
                    }
                }

                final[1] = segundodigito;

                return final;
            }
            else
            {
                throw new Exception("Número de cuenta no válido");
            }
        }

        //static bool CompruebaArray(int[] a)
        //{
        //    int i;
        //    bool correcto = true;

        //    for (i = 0; i < a.Length; i++)
        //    {
        //        if(a[i])
        //    }

        //    return correcto;
        //}

        static bool ValidaCuentaCorriente(int[] a)
        {
            int[] cuenta = CalculaDigitosControl(a);
            //int[] digitos = new int[2];

            //digitos[0] = a[8];
            //digitos[1] = a[9];

            if(cuenta[0] == a[8] && cuenta[1] == a[9])
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static void Main(string[] args)
        {
            int[] a = new int[2];
            int[] b = {1,2,0,6,8,1,3,2,7,4,0,0,0,0,1,9,5,4,3,2};
            a = CalculaDigitosControl(b);
            Console.WriteLine(ValidaCuentaCorriente(b));

            Console.Write(a[0] + ", ");
            Console.WriteLine(a[1]);
            Console.ReadKey();

        }
    }
}
