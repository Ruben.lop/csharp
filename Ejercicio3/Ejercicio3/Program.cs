﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Pokemon p = new Pokemon(250, "Pikachu", "Electrico", 50, 25, 250, 100, 75, 150, "Rayo", true);

            //Console.WriteLine(p.ToString());
            Pokedex p = new Pokedex("pokemon.csv");
            int opcion = -1;

            while (opcion != 0)
            {
                Console.Clear();
                Console.WriteLine("MENU");
                Console.WriteLine("====");
                Console.WriteLine();
                Console.WriteLine("1- Capturar Pokemon");
                Console.WriteLine("2- Ver Pokemon");
                Console.WriteLine("3- Mostrar Porcentaje de Capturas");   
                Console.WriteLine("==========================");
                Console.WriteLine();
                Console.WriteLine("Salir = 0");
                opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1: Console.Clear(); Console.WriteLine("Qué Pokemon Quieres Capturar?"); string pokemon = Console.ReadLine(); p.CapturaPokemon(pokemon); Console.WriteLine("Captura Realizada !!!"); Console.ReadKey(); break;
                    case 2: Console.Clear(); Console.WriteLine("Qué Pokemon Quieres Ver?"); string pokemonVer = Console.ReadLine(); Console.Clear(); Console.WriteLine(p.BuscaPokemon(pokemonVer).ToString()); Console.ReadKey(); break;
                    case 3: Console.Clear(); double media = p.PorcentajeCapturas(); Console.WriteLine("Porcentaje de Captura: " + media.ToString());  Console.ReadKey(); break;
                }
            }

            p.GuardaCSV("pokemon.csv");

            Console.ReadKey();
        }
    }
}
